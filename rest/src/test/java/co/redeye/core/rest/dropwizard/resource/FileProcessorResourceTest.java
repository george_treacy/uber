package co.redeye.core.rest.dropwizard.resource;

import co.redeye.core.service.objects.S3DocumentRequest;
import co.redeye.core.service.objects.MetaData;
import com.google.gson.Gson;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Cool Beans
 * Created by george on 13/09/15.
 */
public class FileProcessorResourceTest {
    private static final Logger logger = LoggerFactory.getLogger(FileProcessorResourceTest.class);

    private static final String jsonRequest = "{ \"bucket\": \"dev-redeye-au\", \"prefix\": " +
            "\"george\", \"filename\": \"test.doc\", \"region\": \"ap-southeast-2\", \"company\": " +
            "\"Alacer\" }";


    private static final String jsonRequest2 = "{ \"bucket\": \"geo-001\", \"prefix\": " +
            "\"\", \"filename\": \"awsgsg-intro.pdf\", \"region\": \"ap-southeast-2\", \"company\": " +
            "\"Alacer\" }";

    @Ignore
    @Test
    public void testGetMetaData() throws Exception {
        logger.debug("boo");

        Gson gson = new Gson();
        S3DocumentRequest request = gson.fromJson(jsonRequest2, S3DocumentRequest.class);
        FileProcessorResource resource = new FileProcessorResource();
        MetaData metaData = resource.getMetaData(request);
        for (String key : metaData.getDocumentProperties().keySet()) {
            System.out.println(key + " " + metaData.getDocumentProperties().get(key));
        }
    }
}