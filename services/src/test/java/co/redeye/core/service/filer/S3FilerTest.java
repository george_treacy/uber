package co.redeye.core.service.filer;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.objects.DocumentRequest;
import co.redeye.core.service.objects.MetaData;
import co.redeye.core.service.objects.S3DocumentRequest;
import co.redeye.core.service.parser.DefaultParser;
import co.redeye.core.service.parser.DocumentScrape;
import co.redeye.core.service.parser.Taxonomy;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import opennlp.tools.util.HashList;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Cool Beans
 * Created by george on 17/09/15.
 */
public class S3FilerTest {

    final String bucket = "georgeit";
    final String key = "arow";
    final String path = "/media/george/Seagate Backup Plus Drive/QUU First Set 150915";

    @Ignore
    @Test
    public void testGetListOfLocalFiles() throws Exception {

        File[] files = new File(path).listFiles();

        Map<String, Integer> mimeCountMap = new HashMap<>();

        for (File file : files) {
            DocumentScrape scrape = new DocumentScrape();
//            scrape.tikaAutoParse(file.getAbsolutePath());
//            System.out.println(scrape.getMetadata().names());
//            System.out.println(scrape.getHandler().toString());
            try {
                final String mime = new Taxonomy().getFileTypeByFile(file.getAbsolutePath());
                System.out.println(mime);
                if (mimeCountMap.containsKey(mime)) {
                    mimeCountMap.replace(mime, mimeCountMap.get(mime) + 1);
                } else {
                    mimeCountMap.put(mime, 1);
                }
            } catch (IOException e) {
                throw new DocumentProcessorException(e);
            }
        }
        System.out.println("************* dump ***************");
        for (String key : mimeCountMap.keySet()) {
            System.out.println(key + " " + mimeCountMap.get(key));
        }
    }

    @Ignore
    @Test
    public void testGetListOfLocalFilesProducer() throws Exception {

        File[] files = new File(path).listFiles();

        Map<String, Integer> producerCountMap = new HashMap<>();

        for (File file : files) {
            DocumentScrape scrape = new DocumentScrape();
            scrape.tikaAutoParse(file.getAbsolutePath());
            final String producer = scrape.getMetadata().get("producer");
            if (producerCountMap.containsKey(producer)) {
                producerCountMap.replace(producer, producerCountMap.get(producer) + 1);
            } else {
                producerCountMap.put(producer, 1);
            }
        }

        System.out.println("************* dump ***************");
        for (String key : producerCountMap.keySet()) {
            System.out.println(key + " " + producerCountMap.get(key));
        }
    }

    @Ignore
    @Test
    public void testGetListOfLocalFilesBodyText() throws Exception {

        File[] files = new File(path).listFiles();

        Integer count = 0;

        System.out.println("Total Number of Files: " + files.length);

        for (File file : files) {
            DocumentScrape scrape = new DocumentScrape();
            scrape.tikaAutoParse(file.getAbsolutePath());
//            System.out.println(scrape.getMetadata().names());
            if (scrape.getHandler().toString() != null && scrape.getHandler().toString().trim().length() > 100) {
                count++;
                System.out.println(scrape.getHandler().toString().trim().length());
            }

        }
        System.out.println("Number of files with body text: " + count);
    }

    @Ignore
    @Test
    public void testGetListOfFiles() throws Exception {
        S3Filer filer = new S3Filer();
        ObjectListing listing = filer.getListOfFiles(bucket, key);
        List<S3ObjectSummary> list = listing.getObjectSummaries();
        for (S3ObjectSummary summary : list) {
            System.out.println("key: " + summary.getKey() + " etag: " + summary.getETag());
            S3DocumentRequest request = new S3DocumentRequest();
            request.setBucket(summary.getBucketName());
            request.setPrefix(summary.getKey());

            String tempFilePath = filer.getLocalFileHandle(request);
            DefaultParser parser = new DefaultParser();
            MetaData metaData = parser.getMetaData(tempFilePath, request);
            try {
                metaData.setMime((new Taxonomy().getFileTypeByFile(tempFilePath)));
            } catch (IOException e) {
                throw new DocumentProcessorException(e);
            }

            System.out.println(metaData.getDocumentProperties().toString());

            filer.deleteLocalFile(tempFilePath);
        }
    }

    @Ignore
    @Test
    public void testGetListOfFiles2() throws Exception {
        S3Filer filer = new S3Filer();
        filer.getListOfFiles(bucket);
    }
}