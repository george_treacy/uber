package co.redeye.core.dagger;

import co.redeye.core.service.parser.DefaultParser;
import co.redeye.core.service.filer.Filer;
import co.redeye.core.service.parser.Parser;
import co.redeye.core.service.filer.S3Filer;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;


@Module()
public class S3DocumentModule {
    @Provides
    @Singleton
    Filer provideFiler() {
        return new S3Filer();
    }

    @Provides
    @Singleton
    Parser provideParser() {
        return new DefaultParser();
    }
}
