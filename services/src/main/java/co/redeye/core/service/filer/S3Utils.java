package co.redeye.core.service.filer;

import co.redeye.core.service.DocumentProcessorException;

import java.io.*;

/**
 * Created on 3/09/2015.
 */
public class S3Utils {

    /**
     * Creates a temporary file with text data to demonstrate uploading a file
     * to Amazon S3
     *
     * @return A newly created temporary file with text data.
     * @throws IOException
     */
    public static File createSampleFile() throws IOException {
        File file = File.createTempFile("aws-java-sdk-", ".txt");
        file.deleteOnExit();

        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        writer.write("01234567890112345678901234\n");
        writer.write("!@#$%^&*()-=[]{};':',.<>/?\n");
        writer.write("01234567890112345678901234\n");
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        writer.close();

        return file;
    }

    /**
     * Displays the contents of the specified input stream as text.
     *
     * @param input The input stream to display as text.
     * @throws IOException
     */
    public static void displayTextInputStream(InputStream input) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        while (true) {
            String line = reader.readLine();
            if (line == null) break;

            System.out.println("    " + line);
        }
        System.out.println();
    }

    public static void handleException(Exception e) throws DocumentProcessorException {
        e.printStackTrace();
        throw new DocumentProcessorException(e);
    }

    public static String getStringFromStream(final InputStream input) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        final StringBuilder builder = new StringBuilder();
        while (true) {
            String line = reader.readLine(); // todo: use a buffer.
            if (line == null) {
                break;
            } else {
                builder.append(line);
            }
        }
        return builder.toString();
    }

}
