package co.redeye.core.service.parser;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.filer.FilerProperties;
import co.redeye.core.service.filer.S3Utils;
import co.redeye.core.service.objects.MetaData;
import co.redeye.core.service.objects.S3DocumentRequest;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;

public class DocumentScrape {

    private static final Logger logger = LoggerFactory.getLogger(DocumentScrape.class);

    private static final String RED_EYE_TEMPLATE = "red_eye_template";
    private static final String ALACER_001 = "alacer_001";

    private String companyId = null;
    private AutoDetectParser parser;
    private BodyContentHandler handler;
    private Metadata metadata;

    public DocumentScrape() {
        this.parser = new AutoDetectParser();
        this.handler = new BodyContentHandler();
        this.metadata = new Metadata();
    }

    /**
     * Main method for deciding how to extract metadata from files.
     */
    public MetaData process(final String filePath, final S3DocumentRequest documentRequest) throws DocumentProcessorException {
        MetaData metaData = new MetaData();
        FilerProperties props = new FilerProperties();
        final String ALACER_ID = props.getProperties().getProperty(ALACER_001);
        final String RED_EYE_ID = "custom:" + props.getProperties().getProperty(RED_EYE_TEMPLATE);;
        // just get the default tika metadata and return it
        tikaAutoParse(filePath);

        String redEyeIdValue = null;
        for (String key : metadata.names()) {
            final String value = metadata.get(key);
            metaData.getDocumentProperties().put(key, value);
            if (RED_EYE_ID.equals(key)) {
                redEyeIdValue = value;
            }
        }

        logger.debug("key: " + RED_EYE_ID + " value: " + redEyeIdValue);

        if (ALACER_ID.equals(redEyeIdValue)) {
            AlacerCoverPageScrape coverPageScrape = new AlacerCoverPageScrape();
            metaData = coverPageScrape.parseAlacer(filePath, metaData, handler.toString());
        }

        return metaData;
    }

    public void tikaAutoParse(final String filePath) throws DocumentProcessorException {
        try (FileInputStream stream = new FileInputStream(filePath)) {
            try {
                logger.debug("parsing: " + filePath);
                parser.parse(stream, handler, metadata);
            } catch (SAXException e) {
                // just keep going. most likely the file is too big.
                logger.error(e.getMessage());
            } catch (TikaException e) {
                // we can get all sorts of weird exceptions here and we need to catch it,
                // log it and keep processing.
                logger.error(e.getMessage());
            }
        } catch (IOException e) {
            S3Utils.handleException(e);
        }
    }

    public BodyContentHandler getHandler() {
        return handler;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void resetParser() {
        this.handler = new BodyContentHandler();
        this.metadata = new Metadata();
    }

}
