package co.redeye.core.service.parser;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.objects.DocumentRequest;
import co.redeye.core.service.objects.S3DocumentRequest;
import co.redeye.core.service.objects.MetaData;

/**
 * Cool Beans
 * Created by george on 10/09/15.
 */
public class DefaultParser implements Parser {

    @Override
    public MetaData getMetaData(final String tempFilePath, final DocumentRequest documentRequest) throws DocumentProcessorException {
        // todo: Should this be a factory or use Dep. Injection?
        DocumentScrape scraper = new DocumentScrape();
        // todo: Exception handling at this point.
        return scraper.process(tempFilePath, (S3DocumentRequest)documentRequest);
    }

}
