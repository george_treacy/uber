package co.redeye.core.service.parser;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.objects.DocumentRequest;
import co.redeye.core.service.objects.MetaData;

/**
 * Created by george on 10/09/15.
 */
public interface Parser {

    MetaData getMetaData(final String tempFilePath, final DocumentRequest documentRequest) throws DocumentProcessorException;

}
