package co.redeye.core.service.filer;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.objects.DocumentRequest;
import co.redeye.core.service.objects.S3DocumentRequest;
import co.redeye.core.service.objects.S3File;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

public class S3Filer implements Filer {

    private static final Logger logger = LoggerFactory.getLogger(S3Filer.class);

    private final AmazonS3 s3;

    public S3Filer() {
        s3 = new AmazonS3Client();
        s3.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_2)); // most likely default region for redeye docs???
    }

    @Override
    public String getLocalFileHandle(DocumentRequest request) throws DocumentProcessorException {
        if (((S3DocumentRequest) request).getRegion() != null) {
            setRegion(((S3DocumentRequest) request).getRegion());
        }
        return copyS3toTemp(((S3DocumentRequest) request));
    }

    private void setRegion(final String region) {
        s3.setRegion(Region.getRegion(Regions.fromName(region)));
    }

    /**
     * Retrieve a document from S3 and store in the local file system.
     * <p>
     * <p>
     * returns a path the temporary file.
     *
     * @return String filePath
     */
    private String copyS3toTemp(final S3DocumentRequest request) throws DocumentProcessorException {

        final String path = "/tmp/s3/";
        final String fileName = UUID.randomUUID().toString();
        final String filePath = path + fileName;

        // todo: make the s3 path name bullet proof with error handling at this point.
        String key = "";

        if (request.getFilename() == null) {
            key = request.getPrefix();
        } else {
            key = request.getPrefix() != null && !"".equals(request.getPrefix())
                    ? request.getPrefix() + "/" + request.getFilename()
                    : request.getFilename();
        }

        logger.debug("key: " + key);

        try {
            S3Object object = s3.getObject(new GetObjectRequest(request.getBucket(), key));
            writeS3ObjectToDisk(object, filePath);
        } catch (AmazonS3Exception e) {
            S3Utils.handleException(e);
        }

        return filePath;
    }

    private void writeS3ObjectToDisk(final S3Object s3Object, final String filePath) throws DocumentProcessorException {
        File file = new File(filePath);
        file.mkdir();
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            int read;
            byte[] bytes = new byte[1024];

            while ((read = s3Object.getObjectContent().read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        } catch (IOException e) {
            S3Utils.handleException(e);
        }
    }

    public void deleteLocalFile(final String filePath) throws DocumentProcessorException {
        Path path = FileSystems.getDefault().getPath(filePath);
        try {
            Files.delete(path);
        } catch (IOException e) {
            S3Utils.handleException(e);
        }
    }

    public ObjectListing getListOfFiles(final String bucket, final String key) {
        return s3.listObjects(bucket, key);
    }

    public ObjectListing getListOfFiles(final String bucket) {
        return s3.listObjects(bucket);
    }
}
