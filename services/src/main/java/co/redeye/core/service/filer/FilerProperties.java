package co.redeye.core.service.filer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Cool Beans
 * Created by george on 18/09/15.
 */
public class FilerProperties {

    private Properties prop;

    public FilerProperties() {
        prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("keys.properties");
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Properties getProperties() {
        return prop;
    }

}
