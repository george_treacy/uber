package co.redeye.core.service;

/**
 * Created on 3/09/2015.
 */
public class DocumentProcessorException extends Exception {

    public DocumentProcessorException(Throwable cause) {
        super(cause);
    }

    public DocumentProcessorException(final String s) {
        super(s);
    }
}
