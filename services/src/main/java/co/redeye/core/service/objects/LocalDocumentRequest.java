package co.redeye.core.service.objects;

/**
 * Created on 2/09/2015.
 */
public class LocalDocumentRequest implements DocumentRequest {

    private String prefix;
    private String filename;
    private String company;

    public LocalDocumentRequest() {
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
